image: python:3.8-slim
  

# Pip's cache doesn't store the python packages
# https://pip.pypa.io/en/stable/reference/pip_install/#caching
#
# If you want to also cache the installed packages, you have to install
# them in a virtualenv and cache it as well.
cache:
  paths:
    - .cache/pip
    - venv/

stages:
  - build
  - test
  - deploy

variables:
  # Change pip's cache directory to be inside the project directory since we can
  # only cache local items.
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"
  TWINE_PASSWORD: ${CI_JOB_TOKEN}
  TWINE_USERNAME: gitlab-ci-token
  TWINE_REPO_URL: https://gitlab.hzdr.de/api/v4/projects/${CI_PROJECT_ID}/packages/pypi


before_script:
  - python -V  # Print out python version for debugging
  - pip install virtualenv
  - virtualenv venv
  - source venv/bin/activate


build:
  stage: build
  script:
    - pip install build
    - python -m build
  artifacts:
    paths:
      - dist/*.whl

test:
  stage: test
  script:
    - pip install ./dist/*.whl
    - pip install -r requirements.txt
    - pip install pytest
    - pytest 


upload_package:
  stage: deploy
  script:
    - pip install twine
    - twine upload --verbose --repository-url $TWINE_REPO_URL dist/*
  rules:
    # don't run on schedules and only on master branch and only if .py or .ipynb files changed
    - if: '$CI_PIPELINE_SOURCE != "schedule" && $CI_COMMIT_BRANCH == "main"'
      changes:
        - "**/*.ipynb"
        - "**/*.py"


pages:
  stage: deploy
  script:
    # mkdocstrings needs to be able to import all dependencies, therefore install
    #  requirements (elements_medialib_toolbox is included in requirements_docs.txt)
    - pip install -r ./requirements_docs.txt
    - mkdocs build --site-dir public
  artifacts:
    paths:
      - public
  rules:
    # don't run on schedules and only on master branch and only if .py or .md files changed
    - if: '$CI_PIPELINE_SOURCE != "schedule" && $CI_COMMIT_BRANCH == "main"'
      changes:
        - "**/*.md"
        - "**/*.py"

