# History

## 0.1.0 (2021-10-21)
  - First release 

## 0.1.2 (2021-12-7)
  - Added pages job for docs
  - moved from GEOMAR Gitlab to https://gitlab.hzdr.de/datahub/marehub/ag-videosimages/elements-medialib-toolbox/

## 0.1.3 (2021-12-14)
 - access assets by filename
 - get and write custom field values

## 0.1.4 (2021-12-16)
  - added cacheing of bundles + assets per media root

## 0.1.5 (2021-12-21)
  - bugfixes
  - write/ read custom fields as dictionaries

## 0.1.6 (2022-01-12)
  - prevent timeouts by getting bundles for media root in smaller chunks

## 0.2.0 (2022-01-31)
 - implement `search_asset` which searches for a single asset, in contrast to `get_asset` which 
   queries all assets of a media root and returns asset from this cache
 - `write_custom_field` and `get_custom_field` using `search_asset` internally, enabling setting 
   custom fields for media roots with a lot of assets (>20000)

## 0.3.0 (2022-02-03)
 - added `search_assets` (plural)
 - results for `search_asset` and `search_assets` limited to assets below a media root
 - removed getting all bundles for a media root to simplify process,`search_asset` now only 
  method of getting assets

## 0.4.0 (2022-03-17)
  - applied patches for new elements API version
  - Support for **Elements-API v3.4** only, support for 3.2 removed, 3.5 untested
  - Known bug in elements API 3.4.1: in case of internal server error for the search_asset method, resolving to `django.core.exceptions.FieldError: Cannot resolve keyword 'root' into field. Choices are: color, id, mediaasset, mediacomment, name, roots, shared` you need to contact elements for a patch
  
## 0.5.0 (2022-06-02)
  - ensure asset objects handled internally instead of asset by name to avoid API roundtrips
  - implemented 'update before write' to enable writing multiple custom fields at once for one asset
  - --> increased performance when importing metadata to custom fields

## 0.6.0 (2022-09-28)
  - added some functionality for handling jobs and tasks
