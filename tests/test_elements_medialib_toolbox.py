#!/usr/bin/env python
"""Tests for `elements_medialib_toolbox` package."""
import os
from cachetools import cached
import elements_sdk
import pytest
import logging
import datetime
import json
import time

from click.testing import CliRunner
from dotenv import load_dotenv

from elements_medialib_toolbox import elements_medialib_toolbox
from elements_medialib_toolbox import cli


@pytest.fixture()
def mgmt_token():
    """Returns bool: True if the elements token has management access to media roots, else False """
    ret = os.environ.get('ELEMENTS_TOKEN_MGMT', False)
    ret = ret and str(ret).lower().strip() != 'false'

    return ret

@pytest.fixture
def api_client():
    load_dotenv()
    assert os.environ.get("ELEMENTS_URL"), "ELEMENTS_URL not set!"
    assert os.environ.get("ELEMENTS_TOKEN"), "ELEMENTS_TOKEN not set!"

    api_client = elements_medialib_toolbox.get_api_client(
        os.environ.get("ELEMENTS_URL"), os.environ.get("ELEMENTS_TOKEN")
    )

    return api_client

@pytest.fixture
def test_asset_filename():
    load_dotenv
    assert os.environ.get(
        "ELEMENTS_TEST_ASSET"
    ), "ELEMENTS_TEST_ASSET not set!"

    return os.environ.get("ELEMENTS_TEST_ASSET")


@pytest.fixture
def test_custom_field_name():
    load_dotenv
    assert os.environ.get(
        "ELEMENTS_TEST_CUSTOM_FIELD"
    ), "ELEMENTS_TEST_CUSTOM_FIELD not set!"

    return os.environ.get("ELEMENTS_TEST_CUSTOM_FIELD")

@pytest.fixture
def test_custom_field_value():
    load_dotenv
    assert os.environ.get(
        "ELEMENTS_TEST_CUSTOM_FIELD_VALUE"
    ), "ELEMENTS_TEST_CUSTOM_FIELD_VALUE not set!"

    return os.environ.get("ELEMENTS_TEST_CUSTOM_FIELD_VALUE")


@pytest.fixture
def test_media_root_name():
    load_dotenv
    assert os.environ.get(
        "ELEMENTS_TEST_MEDIA_ROOT"
    ), "ELEMENTS_TEST_MEDIA_ROOT not set!"

    return os.environ.get("ELEMENTS_TEST_MEDIA_ROOT")


@pytest.fixture
def response():
    """Sample pytest fixture.

    See more at: http://doc.pytest.org/en/latest/fixture.html
    """
    # import requests
    # return requests.get('https://github.com/audreyr/cookiecutter-pypackage')


@pytest.fixture()
def test_job(api_client):
    """ Returns test job name and creates it if it does not exist yet """
    test_job_name = "elements_medialib_toolbox test job"
    job_ids = elements_medialib_toolbox.find_job_ids(api_client,test_job_name)
    if len(job_ids) == 0:
        python_code = ( '# This job does nothing and was created by '
                        'the elements_medialib_toolbox tests for '
                        'testing the handling of jobs remotely via the '
                        'elements sdk.\n'
                        '# You can delete it, it might be recreated '
                        'running the tests though.\n'
                        'import time\n'
                        '\n'
                        'print(sleep_time)\n'
                        '\n'
                        '# keep job running for bit to test looking '
                        'for running jobs\n'
                        'time.sleep(float(sleep_time))')
        elements_medialib_toolbox.create_python_job(api_client,test_job_name,python_code)
    return test_job_name


def test_content(response):
    """Sample pytest test function with the pytest fixture as an argument."""
    # assert 'GitHub' in response.text


def test_command_line_interface():
    """Test the CLI."""
    runner = CliRunner()
    result = runner.invoke(cli.main)
    assert result.exit_code == 0
    assert "elements_medialib_toolbox.cli.main" in result.output
    help_result = runner.invoke(cli.main, ["--help"])
    assert help_result.exit_code == 0
    assert "--help  Show this message and exit." in help_result.output


def test_get_custom_field(api_client, test_custom_field_name):
    assert (
        elements_medialib_toolbox.get_custom_field(api_client, test_custom_field_name)
        is not None
    ), f"Could not find custom field {test_custom_field_name} on {api_client.configuration.host}. Does it exist??"
    assert (
        elements_medialib_toolbox.get_custom_field(
            api_client, test_custom_field_name
        ).name
        == test_custom_field_name
    )

def test_create_custom_field(api_client, mgmt_token):
    if not mgmt_token:
        logging.warning('Elements API Token has insufficent rights for `test_switch_on_custom_field`, skipping!')
        return True
        
    cf_name = "TEST_DELETEME"
    existing = elements_medialib_toolbox.get_custom_field(api_client, cf_name)
    
    if existing is not None:
        deleted = elements_medialib_toolbox.delete_custom_field(api_client, cf_name, are_you_sure=True)
        assert deleted is not None
    
    new_cf = elements_medialib_toolbox.create_custom_field(api_client, cf_name, 'text')
    assert new_cf is not None
    assert new_cf.name == cf_name
    assert (
        elements_medialib_toolbox.get_custom_field(api_client, cf_name)
        is not None
    ), f"Could not find custom field {cf_name} on {api_client.configuration.host}. Does it exist??"
    assert (
        elements_medialib_toolbox.get_custom_field(
            api_client, cf_name
        ).name
        == cf_name
    )
    elements_medialib_toolbox.delete_custom_field(api_client, cf_name, are_you_sure=True)

def test_delete_custom_field(api_client, mgmt_token):
    if not mgmt_token:
        logging.warning('Elements API Token has insufficent rights for `test_switch_on_custom_field`, skipping!')
        return True

    cf_name = "TEST_DELETEME"
    new_cf = elements_medialib_toolbox.get_custom_field(api_client, cf_name)
    if not new_cf:
        new_cf = elements_medialib_toolbox.create_custom_field(api_client, cf_name, 'text')
    assert new_cf

    deleted = elements_medialib_toolbox.delete_custom_field(api_client, cf_name, are_you_sure=True)
    assert deleted is not None
    assert (
        elements_medialib_toolbox.get_custom_field(api_client, cf_name)
        is None
    )


def test_get_media_root(api_client, test_media_root_name):
    assert (
        elements_medialib_toolbox.get_media_root(api_client, test_media_root_name)
        is not None
    ), f"Did not find media_root {test_media_root_name} on {api_client.configuration.host}. Does it exist??"
    assert (
        elements_medialib_toolbox.get_media_root(api_client, test_media_root_name).name
        == test_media_root_name
    )


def test_switch_on_custom_field(api_client, test_media_root_name, test_custom_field_name, mgmt_token):
    if not mgmt_token:
        logging.warning('Elements API Token has insufficent rights for `test_switch_on_custom_field`, skipping!')
        return True

    cf = elements_medialib_toolbox.get_custom_field(api_client, test_custom_field_name)
    mr0 = elements_medialib_toolbox.get_media_root(api_client, test_media_root_name)
    mr_data_b4=mr0.attribute_map.copy()

    # prepare for test
    elements_medialib_toolbox.switch_off_custom_field(
        api_client, test_custom_field_name, test_media_root_name
    )
    mr1 = elements_medialib_toolbox.get_media_root(api_client, test_media_root_name)
    assert (
        cf.id not in [c.id for c in mr1.custom_fields]
    ), "cannot test add_custom_field since remove_custom_field failed!"
    # add cf
    elements_medialib_toolbox.switch_on_custom_field(
        api_client, test_custom_field_name, test_media_root_name
    )

    mr2 = elements_medialib_toolbox.get_media_root(api_client, test_media_root_name)
    assert (
        cf.id in [c.id for c in mr2.custom_fields]
    ), f"Custom field {test_custom_field_name} expected in media root {test_media_root_name} but not found!"

    # assert that only custom_fields were changed
    for k, v in mr2.attribute_map.items():
        if k == 'px_metadata':
            continue  # skip, we expect changes here 
        assert v == mr_data_b4.get(k) # assert current value is same as value before change
            

def test_switch_off_custom_field(api_client, test_media_root_name, test_custom_field_name, mgmt_token):
    if not mgmt_token:
        logging.warning('Elements API Token has insufficent rights for `test_switch_on_custom_field`, skipping!')
        return True
    
    cf = elements_medialib_toolbox.get_custom_field(api_client, test_custom_field_name)
    mr0 = elements_medialib_toolbox.get_media_root(api_client, test_media_root_name)
    mr_data_b4=mr0.attribute_map.copy()
    # prepare for test
    elements_medialib_toolbox.switch_on_custom_field(
        api_client, test_custom_field_name, test_media_root_name
    )
    mr1 = elements_medialib_toolbox.get_media_root(api_client, test_media_root_name)
    assert (
        cf.id in [c.id for c in mr1.custom_fields]
    ), "cannot test switch_off_custom_field since switch_on_custom_field failed!"
    # add cf
    elements_medialib_toolbox.switch_off_custom_field(
        api_client, test_custom_field_name, test_media_root_name
    )
    mr2 = elements_medialib_toolbox.get_media_root(api_client, test_media_root_name)
    assert (
        cf.id not in [c.id for c in mr2.custom_fields]
    ), "cannot test switch_on_custom_field since switch_off_custom_field failed!"
     # assert that only custom_fields were changed
    for k, v in mr2.attribute_map.items():
        if k == 'px_metadata':
            continue  # skip, we expect changes here 
        assert v == mr_data_b4.get(k) # assert current value is same as value before change
            

def test_assure_media_root_obj(api_client, test_media_root_name):
    mr_obj = elements_medialib_toolbox.get_media_root(api_client, test_media_root_name)
    mr_obj_passed = elements_medialib_toolbox._assure_media_root_obj(api_client, mr_obj)
    mr_name_passed = elements_medialib_toolbox._assure_media_root_obj(api_client, mr_obj.name)

    assert isinstance(mr_obj_passed, elements_sdk.model.media_root.MediaRoot)
    assert isinstance(mr_name_passed, elements_sdk.model.media_root.MediaRoot)

def test_search_asset(api_client, test_media_root_name, test_asset_filename):
    asset = elements_medialib_toolbox.search_asset(api_client, test_asset_filename, test_media_root_name)

    assert asset is not None


def test_search_asset_not_found(api_client, test_media_root_name):
    """Tests get_asset with a non-existing asset"""
    asset = elements_medialib_toolbox.search_asset(api_client, "NotTheAssetYouAreLookingFor.png", test_media_root_name)

    assert asset is None

def test_get_custom_field_value(api_client, test_media_root_name, test_asset_filename, test_custom_field_name, test_custom_field_value):
    elements_medialib_toolbox.switch_on_custom_field(api_client, test_custom_field_name, test_media_root_name)
    cf_value = elements_medialib_toolbox.get_custom_field_value(api_client, test_media_root_name, test_asset_filename, test_custom_field_name)

    assert cf_value
    assert test_custom_field_value in cf_value


def test_get_custom_field_value_from_asset_obj(api_client, test_media_root_name, test_asset_filename, test_custom_field_name, test_custom_field_value):
    elements_medialib_toolbox.switch_on_custom_field(api_client, test_custom_field_name, test_media_root_name)
    asset_obj = elements_medialib_toolbox.search_asset(api_client, test_asset_filename, test_media_root_name)
    
    cf_value = elements_medialib_toolbox.get_custom_field_value(api_client, test_media_root_name, 'not evaluated', test_custom_field_name, asset_obj=asset_obj)

    assert cf_value
    assert test_custom_field_value in cf_value


def test_write_custom_field_value_append(api_client, test_media_root_name, test_asset_filename, test_custom_field_name, test_custom_field_value):
    elements_medialib_toolbox.switch_on_custom_field(api_client, test_custom_field_name, test_media_root_name)
    # get current value
    cf_value_old = elements_medialib_toolbox.get_custom_field_value(api_client, test_media_root_name, test_asset_filename, test_custom_field_name)
    # update value
    elements_medialib_toolbox.write_custom_field_value(api_client, test_media_root_name, test_asset_filename, test_custom_field_name, 'FOO', mode='append')
    # get new value
    cf_value_new = elements_medialib_toolbox.get_custom_field_value(api_client, test_media_root_name, test_asset_filename, test_custom_field_name)
    # reset to old value
    elements_medialib_toolbox.write_custom_field_value(api_client, test_media_root_name, test_asset_filename, test_custom_field_name, cf_value_old, mode='replace')

    # compare old and new value
    assert cf_value_new == cf_value_old+'FOO'

def test_write_custom_field_value_replace(api_client, test_media_root_name, test_asset_filename, test_custom_field_name, test_custom_field_value):
    elements_medialib_toolbox.switch_on_custom_field(api_client, test_custom_field_name, test_media_root_name)
    # get current value
    cf_value_old = elements_medialib_toolbox.get_custom_field_value(api_client, test_media_root_name, test_asset_filename, test_custom_field_name)
    # update value
    elements_medialib_toolbox.write_custom_field_value(api_client, test_media_root_name, test_asset_filename, test_custom_field_name, 'FOO', mode='replace')
    # get new value
    cf_value_new = elements_medialib_toolbox.get_custom_field_value(api_client, test_media_root_name, test_asset_filename, test_custom_field_name)
    # reset to old value
    elements_medialib_toolbox.write_custom_field_value(api_client, test_media_root_name, test_asset_filename, test_custom_field_name, cf_value_old, mode='replace')

    # compare old and new value
    assert cf_value_new == 'FOO'

def test_write_custom_field_value_to_asset_object(api_client, test_media_root_name, test_asset_filename, test_custom_field_name, test_custom_field_value):
    elements_medialib_toolbox.switch_on_custom_field(api_client, test_custom_field_name, test_media_root_name)
    asset_obj = elements_medialib_toolbox.search_asset(api_client, test_asset_filename, test_media_root_name)

    # get current value
    cf_value_old = elements_medialib_toolbox.get_custom_field_value(api_client, test_media_root_name, 'not evaluated', test_custom_field_name, asset_obj=asset_obj)
    # update value
    elements_medialib_toolbox.write_custom_field_value(api_client, test_media_root_name, 'not evaluated', test_custom_field_name, 'FOO', mode='replace', asset_obj=asset_obj)
    # get new value
    cf_value_new = elements_medialib_toolbox.get_custom_field_value(api_client, test_media_root_name, test_asset_filename, test_custom_field_name)
    # reset to old value
    elements_medialib_toolbox.write_custom_field_value(api_client, test_media_root_name, test_asset_filename, test_custom_field_name, cf_value_old, mode='replace')

    # compare old and new value
    assert cf_value_new == 'FOO'

def test_write_mulitple_cf(api_client, test_media_root_name, test_asset_filename, test_custom_field_name, mgmt_token):
    """test if changing one cf will affect other cfs"""
    if not mgmt_token:
        logging.warning('Elements API Token has insufficent rights for `test_write_mulitple_cf`, skipping!')
        return True
    # get value of existing cf
    elements_medialib_toolbox.switch_on_custom_field(api_client, test_custom_field_name, test_media_root_name)
    cf_value_unchaged = elements_medialib_toolbox.get_custom_field_value(api_client, test_media_root_name, test_asset_filename, test_custom_field_name)
    
    # create two new cf
    new_cf_a = elements_medialib_toolbox.create_custom_field(api_client, "TEST_DELETEME_A", 'text')
    new_cf_b = elements_medialib_toolbox.create_custom_field(api_client, "TEST_DELETEME_B", 'text')
    elements_medialib_toolbox.switch_on_custom_field(api_client, "TEST_DELETEME_A", test_media_root_name)
    elements_medialib_toolbox.switch_on_custom_field(api_client, "TEST_DELETEME_A", test_media_root_name)
    # change values of new cf one after the othert
    elements_medialib_toolbox.write_custom_field_value(api_client, test_media_root_name, test_asset_filename, "TEST_DELETEME_A", 'FOO', mode='replace')
    elements_medialib_toolbox.write_custom_field_value(api_client, test_media_root_name, test_asset_filename, "TEST_DELETEME_B", 'BAR', mode='replace')
    # get current value of new fields
    cf_value_a = elements_medialib_toolbox.get_custom_field_value(api_client, test_media_root_name, test_asset_filename, "TEST_DELETEME_A")
    cf_value_b = elements_medialib_toolbox.get_custom_field_value(api_client, test_media_root_name, test_asset_filename, "TEST_DELETEME_B")
    # clean up out mess
    elements_medialib_toolbox.delete_custom_field(api_client, "TEST_DELETEME_A", are_you_sure=True)
    elements_medialib_toolbox.delete_custom_field(api_client, "TEST_DELETEME_B", are_you_sure=True)

    # assert that changes to new fields fif not affect exiting field value
    assert cf_value_unchaged == elements_medialib_toolbox.get_custom_field_value(api_client, test_media_root_name, test_asset_filename, test_custom_field_name)
    assert cf_value_a == elements_medialib_toolbox.get_custom_field_value(api_client, test_media_root_name, test_asset_filename, "TEST_DELETEME_A")
    assert cf_value_b == elements_medialib_toolbox.get_custom_field_value(api_client, test_media_root_name, test_asset_filename, "TEST_DELETEME_B")

def test_write_cf_dict_mode_replace(api_client, test_media_root_name, test_asset_filename, mgmt_token):
    """test if changing one cf will affect other cfs"""
    if not mgmt_token:
        logging.warning('Elements API Token has insufficent rights for `test_write_cf_dict_mode_replace`, skipping!')
        return True
    
    # create new cf
    new_cf = elements_medialib_toolbox.create_custom_field(api_client, "TEST_DELETEME_A", 'text')
    elements_medialib_toolbox.switch_on_custom_field(api_client, "TEST_DELETEME_A", test_media_root_name)
    
    # change values of new cf twice, get values
    elements_medialib_toolbox.write_custom_field_value(api_client, test_media_root_name, test_asset_filename, "TEST_DELETEME_A", {'foo': 'bar', 'this': 'that'}, mode='dict_replace')
    cf_value_a = elements_medialib_toolbox.get_custom_field_value(api_client, test_media_root_name, test_asset_filename, "TEST_DELETEME_A")
    elements_medialib_toolbox.write_custom_field_value(api_client, test_media_root_name, test_asset_filename, "TEST_DELETEME_A", {'bar': 'foo', 'that': 'this'}, mode='dict_replace')
    cf_value_b = elements_medialib_toolbox.get_custom_field_value(api_client, test_media_root_name, test_asset_filename, "TEST_DELETEME_A")
    
    # clean up out mess
    elements_medialib_toolbox.delete_custom_field(api_client, "TEST_DELETEME_A", are_you_sure=True)
    elements_medialib_toolbox.delete_custom_field(api_client, "TEST_DELETEME_B", are_you_sure=True)

    # assert that values are parseable as dict, values are as expected
    assert json.loads(cf_value_a).get('foo') == 'bar'
    assert json.loads(cf_value_a).get('this') == 'that'
    assert json.loads(cf_value_b).get('bar') == 'foo'
    assert json.loads(cf_value_b).get('that') == 'this'

def test_write_cf_dict_mode_update(api_client, test_media_root_name, test_asset_filename, mgmt_token):
    # create new cf
    if not mgmt_token:
        logging.warning('Elements API Token has insufficent rights for `test_write_cf_dict_mode_update`, skipping!')
        return True
    new_cf = elements_medialib_toolbox.create_custom_field(api_client, "TEST_DELETEME_A", 'text')
    elements_medialib_toolbox.switch_on_custom_field(api_client, "TEST_DELETEME_A", test_media_root_name)
    
    # change values of new cf twice, get values
    elements_medialib_toolbox.write_custom_field_value(api_client, test_media_root_name, test_asset_filename, "TEST_DELETEME_A", {'foo': 'bar', 'this': 'that'}, mode='dict_update')
    elements_medialib_toolbox.write_custom_field_value(api_client, test_media_root_name, test_asset_filename, "TEST_DELETEME_A", {'foo': 'foobar', 'when': 'now'}, mode='dict_update')
    cf_value = elements_medialib_toolbox.get_custom_field_value(api_client, test_media_root_name, test_asset_filename, "TEST_DELETEME_A")
    
    # clean up out mess
    elements_medialib_toolbox.delete_custom_field(api_client, "TEST_DELETEME_A", are_you_sure=True)
    elements_medialib_toolbox.delete_custom_field(api_client, "TEST_DELETEME_B", are_you_sure=True)

    # assert that values are parseable as dict, values are as expected
    assert json.loads(cf_value).get('foo') == 'foobar'
    assert json.loads(cf_value).get('this') == 'that'
    assert json.loads(cf_value).get('when') == 'now'


def test_find_job(api_client, test_job):
    # find a non-existing job
    job_name = "this job does hopefully not exist"
    job_ids = elements_medialib_toolbox.find_job_ids(api_client,job_name)
    assert len(job_ids) == 0
    
    # find an existing job
    job_ids = elements_medialib_toolbox.find_job_ids(api_client,test_job)
    assert len(job_ids) != 0


def start_test_job(api_client, test_job,sleep_time):
    """ Start test job and returns its id and variables """
    job_name = test_job
    job_ids = elements_medialib_toolbox.find_job_ids(api_client,job_name)
    variables = {'sleep_time':str(sleep_time)}
    task_id = elements_medialib_toolbox.start_job(api_client,job_ids[0],variables)
    return task_id, variables


def test_start_job_and_check(api_client, test_job):
    # start test job
    sleep_time = 5
    task_id, variables = start_test_job(api_client,test_job,sleep_time)
    # give it some time to start running and find running task
    time.sleep(2)
    task_id_running = elements_medialib_toolbox.find_running_task(api_client,test_job)
    assert task_id_running == task_id

    # give it time to finish and find finished task
    time.sleep(sleep_time)
    task_id_finished = elements_medialib_toolbox.find_finished_task(api_client,test_job)
    if not task_id_finished == task_id:
        pytest.skip("Test Job did not finish in time, probably a lot of tasks are currently running on the system. Skipping test on finished tasks.")
        return
    assert task_id_finished == task_id

    # check if task finished
    finished = elements_medialib_toolbox.check_if_task_finished(api_client,task_id)
    assert finished == True


def test_job_exception(api_client, test_job):
    # start test job properly so job does not finish with exception
    task_id, variables = start_test_job(api_client,test_job,sleep_time=0)
    time.sleep(1)
    finished = elements_medialib_toolbox.check_if_task_finished(api_client,task_id)
    if not finished == True:
        pytest.skip("Test Job did not finish in time, probably a lot of tasks are currently running on the system. Skipping test on task exceptions.")
        return
    assert finished == True
    exception = elements_medialib_toolbox.get_task_exception(api_client,task_id)
    assert exception is None

    # start job with invalid variable so it raises an exception
    task_id = task_id, variables = start_test_job(api_client,test_job,sleep_time='this will raise an exception')
    time.sleep(1)
    finished = elements_medialib_toolbox.check_if_task_finished(api_client,task_id)
    assert finished == True
    exception = elements_medialib_toolbox.get_task_exception(api_client,task_id)
    assert exception is not None