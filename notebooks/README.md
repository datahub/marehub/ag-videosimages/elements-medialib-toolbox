# Elements automations and workflows
This is a place to store automations and workflows
for elements in the form of Jupyter Notebooks
or standalons scripts.

Please use this space predominantly for automations of general (i.e. MareHub)
interest and store automations that are only relevant for your organisation in
a seperate repository.

If you define new functions which can be beneficial for others, please consider
**contributing** them **to the ElementsMedialibToolbox** by moving them out of your notebooks
and into the python package hosted in this repo.

# Jupyter Notebook or Elements Job?
Jupyter Notebooks are especially suited for one-off tasks or tasks that are very complex and
require a lot of experimentation and interaction with the ElementsMedialibToolbox.

For very simple tasks or tasks that need fast access to the storage backens, a job in elements is more suited. Jobs can also be started from the context menue of the WebUI and the Elements clients,
making it easy to distribute to a wide audience.

It is entirely feasible to start implementing an automation as a Notebook and later migrating it to a job
in elements.


