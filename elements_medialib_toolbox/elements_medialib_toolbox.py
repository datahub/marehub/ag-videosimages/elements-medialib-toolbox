"""Main module."""
import logging
import json

import elements_sdk
from elements_sdk.api import automation_api, media_library_api
from elements_sdk.model.job_update import JobUpdate
from elements_sdk.model.media_root_detail_partial_update import MediaRootDetailPartialUpdate
from elements_sdk.model.asset_partial_update import AssetPartialUpdate
from elements_sdk.model.start_job_request import StartJobRequest
from elements_sdk.model.subtask_update import SubtaskUpdate


# info below was copy-pasted from medialib site code
#  TODO ask elements for programmatic way to get cf types
custom_field_types = [
    "text",  # Text field
    "large-text",  # Large text field
    "select",  # Selection field
    "checkbox",  # Checkbox field
    "number",  # Number field
    "date",  # Date field
    "user",  # User selector
    "path",  # Path selector
    "url",  # URL field
]


def get_api_client(elements_url: str, api_token: str):
    """Creates api_client instance with provided credentials
    Params:
        elements_url str: Base url of your elements-medialib instance
        api_token str: Access token for your elements-medialib instance
    """
    
    configuration = elements_sdk.Configuration(
            host=elements_url,
            discard_unknown_keys=True,
    )
    
    configuration.client_side_validation = False
    configuration.api_key['Bearer'] = f'Bearer {api_token}'

    # Enter a context with an instance of the API client
    with elements_sdk.ApiClient(configuration) as api_client:
        return api_client


def get_custom_field(api_client: "elements_sdk.ApiClient", cf_name: str):
    """Returns CustomField object w/ matching name or None if not found
    Params:
        api_client elements_sdk.ApiClient: elements_sdk.ApiClient instance configured w/ url and credentials,
                see [get_api_client(elements_url, api_token)][elements_medialib_toolbox.elements_medialib_toolbox.get_api_client]
        cf_name str: Name of (an existing) custom field
    """
    api_instance = media_library_api.MediaLibraryApi(api_client)

    for cf in api_instance.get_all_custom_fields():
        if cf.name == cf_name:
            return cf

    return None


def get_media_root(api_client: "elements_sdk.ApiClient", media_root_name):
    """Returns MediaRoot object w/ matching name or None if not found
    Params:
        api_client elements_sdk.ApiClient: elements_sdk.ApiClient instance configured w/ url and credentials,
            see [get_api_client(elements_url, api_token)][elements_medialib_toolbox.elements_medialib_toolbox.get_api_client]
        media_root_name str: Name of (an existing) Media Root
    """
    api_instance = media_library_api.MediaLibraryApi(api_client)

    for mr in api_instance.get_all_media_roots():
        if mr.name == media_root_name:
            return mr
    return None


def switch_on_custom_field(
    api_client: "elements_sdk.ApiClient", cf_name, media_root_name
):
    """Switches on/ adds a custom field to a media root
    Params:
        api_client elements_sdk.ApiClient: elements_sdk.ApiClient instance configured w/ url and credentials,
            see [get_api_client(elements_url, api_token)][elements_medialib_toolbox.elements_medialib_toolbox.get_api_client]
        cf_name str: Name of an existing custom field
        media_root_name str: Name of an existing Media Root
    """
    api_instance = media_library_api.MediaLibraryApi(api_client)

    cf = get_custom_field(api_client, cf_name)
    mr = get_media_root(api_client, media_root_name)

    assert cf, f"CustomField with name {cf_name} not found!"
    assert mr, f"MediaRoot with name {media_root_name} not found!"

    if cf.id in [c.id for c in mr.custom_fields]:
        return  # already there

    data = MediaRootDetailPartialUpdate()
    data.custom_fields = mr.custom_fields
    data.custom_fields.append(cf)

    api_instance.patch_media_root(mr.id, data)


def switch_off_custom_field(
    api_client: "elements_sdk.ApiClient", cf_name: str, media_root_name: str
):
    """Switches off/ removes a custom field from a media root
    Params:
        api_client elements_sdk.ApiClient: elements_sdk.ApiClient instance configured w/ url and credentials
            see [get_api_client(elements_url, api_token)][elements_medialib_toolbox.elements_medialib_toolbox.get_api_client]
        cf_name str: Name of an existing custom field
        media_root_name str: Name of an existing Media Root
    """
    api_instance = media_library_api.MediaLibraryApi(api_client)
    cf = get_custom_field(api_client, cf_name)
    mr = get_media_root(api_client, media_root_name)

    assert cf, f"CustomField with name {cf_name} not found!"
    assert mr, f"MediaRoot with name {media_root_name} not found!"

    if cf.id not in [c.id for c in mr.custom_fields]:
        return  # already there

    data = MediaRootDetailPartialUpdate()
    data.custom_fields = [c for c in mr.custom_fields if c.id is not cf.id]
    
    api_instance.patch_media_root(mr.id, data)


def create_custom_field(
    api_client: "elements_sdk.ApiClient", cf_name: str, cf_type: str, **kwargs
):
    """Creates a custom field if it does not already exists
    Params:
      api_client elements_sdk.ApiClient: elements_sdk.ApiClient instance configured w/ url and credentials,
        see see [get_api_client(elements_url, api_token)][elements_medialib_toolbox.elements_medialib_toolbox.get_api_client]
      cf_name str: designated name of new custom field
      cf_type str: designated type of new custom field, slect from known types
      **kwargs: any additional matching data is passed to the create_custum_field API call
    """

    ret = get_custom_field(api_client, cf_name)
    if not ret:
        assert (
            cf_type in custom_field_types
        ), f"cf_type {cf_type} unknwn! Choose from {custom_field_types}"
        cf_data = elements_sdk.CustomField(options=[], labels=[], name=cf_name, type=cf_type)
        
        api_instance = media_library_api.MediaLibraryApi(api_client)
        ret = api_instance.create_custom_field(cf_data)

    return ret


def delete_custom_field(
    api_client: "elements_sdk.ApiClient", cf_name, are_you_sure=False
):
    """Deletes a custom field if it does already exists
     Params:
      api_client elements_sdk.ApiClient:  elements_sdk.ApiClient instance configured w/ url and credentials,
    see see [get_api_client(elements_url, api_token)][elements_medialib_toolbox.elements_medialib_toolbox.get_api_client]
      cf_name str: Name of custom field to be deleted
      are_you_sure boolean: to prevent dumb stuff
    """
    if not are_you_sure:
        return False

    ret = get_custom_field(api_client, cf_name)
    if ret:
        api_instance = media_library_api.MediaLibraryApi(api_client)
        # no news is good news since 3.4, the delete_custom_field does not return anything anymore
        api_instance.delete_custom_field(ret.id)

    return True

def _assure_media_root_obj(api_client, media_root):
    """Helper method which makes accepts either a media root name or a media root object and always returns the object.
    api_client elements_sdk.ApiClient:  elements_sdk.ApiClient instance configured w/ url and credentials,
      see see [get_api_client(elements_url, api_token)][elements_medialib_toolbox.elements_medialib_toolbox.get_api_client]
    media_root  str or elements_sdk.model.media_root.MediaRoot: Name of media root for the asset or MediaRoot obj
    """
    if isinstance(media_root, str):
        logging.debug(f"getting media root obj for {media_root}")
        media_root = get_media_root(api_client, media_root)

    assert isinstance(
        media_root, elements_sdk.model.media_root.MediaRoot
    ), f"Could not resolve MediaRoot {media_root}!"

    return media_root


def search_assets(api_client, asset_filename, media_root):
    """Performs server side searche for assets by filename and name of media root.

     Params:
      api_client elements_sdk.ApiClient:  elements_sdk.ApiClient instance configured w/ url and credentials,
        see see [get_api_client(elements_url, api_token)][elements_medialib_toolbox.elements_medialib_toolbox.get_api_client]
      asset_filename str: complete filename of the asset in question.
      media_root: Name of the media root or media root object

    Returns list with found asset objects.
    """
    ret = None
    mr = _assure_media_root_obj(api_client, media_root=media_root)

    api_instance = media_library_api.MediaLibraryApi(api_client)
   
    ret = api_instance.get_all_assets(display_name=asset_filename, for_root=mr.id)

    if not ret:
        logging.info(f"failed to find asset w/ filename {asset_filename}!")

    return ret

def search_asset(api_client, asset_filename, media_root):
    """Performs server side search for a single asset by its's filename and name of media root.
      Returns first found, i.e assumes that there is only one asset of
     the given filename under the supplied media_root.

     Params:
      api_client elements_sdk.ApiClient:  elements_sdk.ApiClient instance configured w/ url and credentials,
        see see [get_api_client(elements_url, api_token)][elements_medialib_toolbox.elements_medialib_toolbox.get_api_client]
      asset_filename str: complete filename of the asset in question.
      media_root: Name of the media root or media root object

    Returns asset object or None.
    """
    ret = search_assets(api_client, asset_filename, media_root)

    if not ret or len(ret) < 1:
        return None
    
    return ret[0]

def get_custom_field_value(
    api_client, media_root, asset_name, custom_field_name, asset_obj=None
):
    """Looks up custom field vaule for an asset under a media root

    Params:
      api_client elements_sdk.ApiClient:  elements_sdk.ApiClient instance configured w/ url and credentials,
         see see [get_api_client(elements_url, api_token)][elements_medialib_toolbox.elements_medialib_toolbox.get_api_client]
      media_root str or elements_sdk.model.media_root.MediaRoot: Name of media root for the asset or MediaRoot obj
      asset_name str: (File-)Name of asset
      custom_field_name str: Name of custom field
      asset_obj elements_sdk.model.asset.Asset: asset object. If not None, asset_name is ignored

    Returns value of custom field or None if not found
    """
    if asset_obj and isinstance(asset_obj, elements_sdk.model.asset.Asset):
        asset =  asset_obj
    else:
        asset = search_asset(api_client, asset_name, media_root)
    if not asset:
        logging.warning(f"Did not find asset {asset_name} under media root {media_root}")
        return None  

    cf_value = _get_cf_value(asset, custom_field_name)
    if cf_value:
        return asset.custom_fields[custom_field_name]

    else:
        logging.info(f"Custom field {custom_field_name} not set for asset {asset_name}")
        return None

def _get_cf_value(asset, custom_field_name):
    if custom_field_name in asset.custom_fields.keys():
        return asset.custom_fields[custom_field_name]
    else:
        return None


def get_custom_field_value_as_dict(
    api_client, media_root, asset_name, custom_field_name, asset_obj=None
):
    """Looks up custom field vaule for an asset under a media root and attempts to parse it as a dictionary

    Params:
      api_client elements_sdk.ApiClient:  elements_sdk.ApiClient instance configured w/ url and credentials,
         see see [get_api_client(elements_url, api_token)][elements_medialib_toolbox.elements_medialib_toolbox.get_api_client]
      media_root str or elements_sdk.model.media_root.MediaRoot: Name of media root for the asset or MediaRoot obj
      asset_name str: (File-)Name of asset
      custom_field_name str: Name of custom field
      asset_obj elements_sdk.model.asset.Asset: asset object. If not None, asset_name is ignored

    Returns value of custom field parsed to dict or None if not found or not parseable
    """
    if not asset_obj or not isinstance(asset_obj, elements_sdk.model.asset.Asset):
        asset_obj = search_asset(api_client=api_client, 
                                asset_filename=asset_name, 
                                custom_field_name=custom_field_name, 
                                media_root=media_root)
    
    return _get_cf_value_as_dict(asset_obj, custom_field_name)

def _get_cf_value_as_dict(asset, custom_field_name):
    ret = _get_cf_value(asset=asset, custom_field_name=custom_field_name)
    if not ret or ret == "":
        ret = "{}"
    try:
        ret = json.loads(ret)
    except:
        try:  # json is only valid w/ double quotes, attempts to replace single quotes
            ret = ret.replace("'", '"')
            ret = json.loads(ret)
        except:
            logging.error(f"Unable parse field {custom_field_name} to dict, value: {ret}")
            return None

    assert isinstance(
        ret, dict
    ), f"Base type of dict modes fields needs to be dict, but we found this type: {type(ret)}"
    return ret

def update_custom_field_value(asset_obj, custom_field_name, custom_field_value, mode="append"):
    """ Updates custom field data coording to the `mode` parameter. Changes are only local,
    **does not persist changes to Elements**
    Params:
      asset_obj elements_sdk.model.asset.Asset: Asset object
      custom_field_name str: Name of custom field
      custom_field_value str: Value to be set for custom field
      mode str: Either `append`, `replace`, or `dict`
         - `append` will append new value to old value if the new value is not already contained on current value
         - `replace` will completely replace old value with new value
         - `dict_update` will attempt to parse old_value and new value as a dictionary (json syntax expected) and add/ updates the fields of old_value.
           Will do nothing and return None if parsing of old or new value fails.
         - `dict_replace` will attempt to parse new value as a dictionary (json syntax expected) replaces old value with str representation of new value
           Will do nothing and return None if parsing of new value fails.

    Returns updated asset if update was successfull or False
    """
    # catch dict modes
    if mode == "dict_update":
        return _update_custom_field_value_dict_mode(
            asset_obj,
            custom_field_name,
            custom_field_value,
            mode="update"
        )
    if mode == "dict_replace":
        return _update_custom_field_value_dict_mode(
            asset_obj,
            custom_field_name,
            custom_field_value,
            mode="replace",
        )

    if mode == "replace":
        new_value = custom_field_value
    else:  # assume 'append' mode
        old_value = _get_cf_value(asset_obj, custom_field_name) or ""
        
        if custom_field_value in old_value:
            logging.info(
                f"No need to append new value {custom_field_value} to custom field {custom_field_name}; alrady present."
            )
            new_value = old_value
        else:
            new_value = old_value + custom_field_value

    asset_obj.custom_fields[custom_field_name] = new_value
    return asset_obj
    


def write_custom_field_value(
    api_client,
    media_root,
    asset_name,
    custom_field_name,
    custom_field_value,
    mode="append",
    asset_obj=None
):
    """Updates a single custom field vaule for an asset under a media root
     For multiple changes on the same asset, consider calling 
     [update_custom_field_value(asset_obj, custom_field_name, custom_field_value, mode="append")][elements_medialib_toolbox.elements_medialib_toolbox.update_custom_field_value]
     multiple times followed by 
     [write_custom_field_values(api_client, asset_obj)][elements_medialib_toolbox.elements_medialib_toolbox.write_custom_field_values]
    Params:
      api_client elements_sdk.ApiClient:  elements_sdk.ApiClient instance configured w/ url and credentials,
         see see [get_api_client(elements_url, api_token)][elements_medialib_toolbox.elements_medialib_toolbox.get_api_client]
      media_root str or elements_sdk.model.media_root.MediaRoot: Name of media root for the asset or MediaRoot obj
      asset_name str: (File-)Name of asset
      custom_field_name str: Name of custom field
      custom_field_value str: Value to be set for custom field
      mode str: Either `append`, `replace`, or `dict`
         - `append` will append new value to old value if the new value is not already contained on current value
         - `replace` will completely replace old value with new value
         - `dict_update` will attempt to parse old_value and new value as a dictionary (json syntax expected) and add/ updates the fields of old_value.
           Will do nothing and return None if parsing of old or new value fails.
         - `dict_replace` will attempt to parse new value as a dictionary (json syntax expected) replaces old value with str representation of new value
           Will do nothing and return None if parsing of new value fails.
      asset_obj elements_sdk.model.asset.Asset: asset object. If not None, asset_name is ignored

    Returns updated asset if update was successfull or False
    """
    if not asset_obj or not isinstance(asset_obj, elements_sdk.model.asset.Asset):
        asset_obj = search_asset(api_client, asset_name, media_root)

    asset_obj = update_custom_field_value(asset_obj=asset_obj, 
                            custom_field_name=custom_field_name, 
                            custom_field_value=custom_field_value, 
                            mode=mode)

    return write_custom_field_values(
        api_client,
        asset_obj
    )


def _update_custom_field_value_dict_mode(
    asset_obj,
    custom_field_name,
    custom_field_value,
    mode="update",
):
    """Updates a custom field value in 'dict' mode. Changes are only local,
    **does not persist changes to Elements**
    Params:
      asset_obj elements_sdk.model.asset.Asset: Asset object
      custom_field_name str: Name of custom field
      custom_field_value str: Value to be set for custom field
      mode str: Either `update`, or `replace`
         - `update` will attempt to parse old_value and new value as a dictionary (json syntax expected) and add/ updates the fields of old_value.
           Will do nothing and return None if parsing of old or new value fails.
         - `replace` will attempt to parse new value as a dictionary (json syntax expected) replaces old value with str representation of new value
           Will do nothing and return None if parsing of new value fails.

    Returns updated asset if update was successfull or False
    """
    # 1st make sure we can parse the new value as dict
    if isinstance(custom_field_value, dict):
        new_value = custom_field_value
    else:
        new_value = {}
        try:
            new_value = json.loads(custom_field_value)
        except:
            logging.error(
                f"Unable to parse custom field value to dict: {custom_field_value}"
            )
            return None

    assert isinstance(
        new_value, dict
    ), f"Base type of dict modes fields needs to be dict, but you passed this: {new_value}"

    # if old value is to be updated, we will make sure old value can also be parsed
    if mode != "replace":
        old_val = _get_cf_value_as_dict(asset=asset_obj, custom_field_name=custom_field_name) or {}
        # OK, now we have two dicts and can finally update one with the other
        old_val.update(new_value)
        new_value = old_val    

    asset_obj.custom_fields[custom_field_name] = json.dumps(new_value)
    return asset_obj
    

def write_custom_field_values(
    api_client, asset_obj
):
    """Writes changed custom field values using the elements API. Called by the higher level functions or manually
    after a number of `update_custom_field` operations on `asset_obj`."""
    
    if not asset_obj or not isinstance(asset_obj, elements_sdk.model.asset.Asset):
        asset = asset_obj
        logging.error(
            f"cannot update custom field for asset {asset_obj}"
        )
        return False

    asset = asset_obj
    asset_data = AssetPartialUpdate()
    asset_data.custom_fields = asset.custom_fields

    api_instance = media_library_api.MediaLibraryApi(api_client)
    return api_instance.patch_asset(asset.id, asset_data)

    
def create_python_job(api_client: elements_sdk.ApiClient,job_name:str,python_code:str):
    """ Create a job with a python subtask. Returns elements_sdk.model.job.Job object """
    api_instance = automation_api.AutomationApi(api_client)
    createdJob = None
    job = JobUpdate(name=job_name, allow_others_to_start=True) # Job | 
    subtask_args = {'cmd':python_code,
                    'external_process': 'False',
                    'output_names': '{}',
                    'output_types': '{}'}

    try:
        createdJob = api_instance.create_job(job)
        subtask = SubtaskUpdate( parent = createdJob.id, #This task requires the "Admin access" permission
                                            kwargs=subtask_args,
                                            graph_layout={}, 
                                            log_variable=False, 
                                            name='Python',
                                            no_concurrency=False,
                                            noop_dont_save=False,
                                            sync=False,
                                            task='script.python') # Subtask |
        api_response = api_instance.create_subtask(subtask)
    except elements_sdk.ApiException as e:
        logging.error("Exception when calling AutomationApi->create_job: %s\n" % e)
    return createdJob


def find_jobs(api_client: elements_sdk.ApiClient,job_name:str):
    """ Looks for job job_name and returns a list of found elements_sdk.model.job.Job objects """
    ret_jobs = []
    api_instance = automation_api.AutomationApi(api_client)

    try:
        api_response = api_instance.get_all_jobs(special_type__isnull='true')
    except elements_sdk.ApiException as e:
        msg = "Exception when calling AutomationApi->get_all_jobs: %s\n" % e
        return ret_jobs

    for job in api_response:
        if job.name == job_name:
            ret_jobs.append(job)

    return ret_jobs


def find_job_ids(api_client: elements_sdk.ApiClient,job_name:str):
    """ Looks for job job_name and returns a list of ids of the found jobs. """
    found_jobs = find_jobs(api_client,job_name)
    job_ids = [job.id for job in found_jobs]
    return job_ids


def start_job(api_client: elements_sdk.ApiClient,job_id:int,variables:dict):
    """ Starts job with variables and returns id of the created task. """
    api_instance = automation_api.AutomationApi(api_client)
    task_id = -1
    data = StartJobRequest(variables=variables) # StartJobRequest | 
    try:
        api_response = api_instance.start_job(job_id, data)
        task_id = api_response[0].id
    except elements_sdk.ApiException as e:
        logging.error("Exception when calling AutomationApi->start_job: %s\n" % e)
    return task_id


def find_running_task(api_client: elements_sdk.ApiClient,job_name:str,):
    """ Looks for running task created from job_name. Returns (first) task id if found, otherwise -1. """
    api_instance = automation_api.AutomationApi(api_client)
    state = '1' # str | Filter the returned list by `state`. (optional)
    try:
        api_response = api_instance.get_all_tasks(state=state)
        for task in api_response:
            if task.name == job_name and task.is_running: 
                # is_running should always be the case since we filtered by state==1==running
                return task.id
                
    except elements_sdk.ApiException as e:
        logging.error("find_running_task: Exception when calling AutomationApi->get_all_tasks: %s\n" % e)
    return -1


def find_finished_task(api_client: elements_sdk.ApiClient,job_name:str):
    """ Looks for finished task created from job_name. Returns (first) task id if found, otherwise -1. """
    api_instance = automation_api.AutomationApi(api_client)
    # if response is too long we get an error
    return_limit = 50
    offset = 0
    stop = False
    while not stop:
        #print(offset)
        try:
            api_response = api_instance.get_finished_tasks(limit=return_limit,offset=offset)
            if len(api_response) == 0:
                stop = True
            for task in api_response:
                if task.name == job_name and task.state > 1 and task.is_finished:
                    return task.id
                    
        except elements_sdk.ApiException as e:
            logging.error("find_finished_task: Exception when calling AutomationApi->get_all_tasks: %s\n" % e)
        offset += return_limit
    return -1


def check_if_task_finished(api_client: elements_sdk.ApiClient,task_id:str):
    """ Returns whether task finished. """
    api_response = get_task(api_client,task_id)
    # while task is running: 
    #   exception: None
    #   state: 1
    #   progress: {'current': None, 'max': None, 'message': None}
    # if task finished successfully:
    #   exception: None
    #   state: 2
    #   progress {'current': 0, 'max': 1, 'message': 'Finishing'}
    # if task finished with exception:
    #   exception: Exception('test')
    #   state: 5
    #   progress {'current': 0, 'max': 1, 'message': 'Finishing'}
    if api_response.state > 1:
        return True
    else:
        return False


def get_task_exception(api_client: elements_sdk.ApiClient,task_id:str):
    """ Returns task exception or None if successful. CAUTION: check if task finished in the first place. """
    return get_task(api_client,task_id).exception


def get_task(api_client: elements_sdk.ApiClient,task_id:str):
    """ Returns task object """
    api_instance = automation_api.AutomationApi(api_client)
    return api_instance.get_task(task_id)


