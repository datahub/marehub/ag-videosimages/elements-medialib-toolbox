"""Top-level package for Elements Medialib Toolbox."""

__author__ = """Claas Faber"""
__email__ = "cfaber@geomar.de"
__version__ = "__version__ = '0.5.0'"
