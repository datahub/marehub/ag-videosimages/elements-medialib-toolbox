#!/usr/bin/env python

"""The setup script."""

from setuptools import setup, find_packages

with open('README.md') as readme_file:
    readme = readme_file.read()

with open('HISTORY.md') as history_file:
    history = history_file.read()

requirements = ['Click>=7.0', 'elements-sdk>=23', 'requests', 'python-dotenv', 'cachetools']

test_requirements = ['pytest>=3', ]

setup(
    author="Claas Faber",
    author_email='cfaber@geomar.de',
    python_requires='>=3.6',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
    ],
    description="Collection of tools for working with the Elements Medialib API/SDK within the context of MareHub",
    entry_points={
        'console_scripts': [
            'elements_medialib_toolbox=elements_medialib_toolbox.cli:main',
        ],
    },
    install_requires=requirements,
    license="BSD license",
    long_description=readme + '\n\n' + history,
    include_package_data=True,
    keywords='elements_medialib_toolbox',
    name='elements_medialib_toolbox',
    packages=find_packages(include=['elements_medialib_toolbox', 'elements_medialib_toolbox.*']),
    test_suite='tests',
    tests_require=test_requirements,
    url='https://git.geomar.de/path-to-your-project',
    version='0.6.0',
    zip_safe=False,
)
