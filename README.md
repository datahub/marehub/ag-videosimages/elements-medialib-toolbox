# Elements Medialib Toolbox 

Collection of tools for working with the Elements Medialib API/SDK within the context of MareHub

 - Free software: BSD license 
 - Documentation: https://datahub.pages.hzdr.de/marehub/ag-videosimages/elements-medialib-toolbox/elements_medialib_toolbox-reference/


# Features

 - Simplified handling of custom fields
 - Simplified retrieval of workspaces and assets
 - Simplified handling of jobs and tasks

# Credits

This package was created with
[Cookiecutter](https://github.com/audreyr/cookiecutter) and the
[open-source/toolbox/cookiecutter-pypackage](https://git.geomar.de/open-source/toolbox/cookiecutter-pypackage)
project template.
